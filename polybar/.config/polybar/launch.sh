#!/usr/bin/env sh

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

polybar example&
polybar VGA&
polybar HDMI&
polybar XWAYLAND0&
# polybar XWAYLAND1&

echo "Bars launched..."