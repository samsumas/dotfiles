alias rm='rm -I'
alias info='info --vi-keys'
alias maxima='rlwrap maxima'
alias octave='rlwrap octave'
alias univpn='sudo openconnect -u s8sashal asa2.uni-saarland.de'
alias pacman='sudo pacman'
alias systemctl="sudo systemctl"
alias pwd="pwd -P"
alias mosml='rlwrap mosml'
alias mount="udisks -p"
alias lldb="rlwrap lldb"
alias vim="nvim"
alias curl="curl -JLO"
alias scan="scanimage --device pixma:04A926A3_SDF6Z0194776O --format=png --resolution 150"
alias jn="jupyter notebook"
alias less="less -R"
