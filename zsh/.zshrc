# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

source ~/.promptline.sh

#load aliases
. ~/.bash_aliases

#winethingies
export WINEPREFIX='/home/sami/.wine32'

# Path to your oh-my-zsh installation.
export ZSH=/home/sami/.oh-my-zsh
export EDITOR=nvim

# Gurobi-fix
export GUROBI_HOME=$HOME/gurobi/gurobi911/linux64
export PATH="${GUROBI_HOME}/bin:${PATH}"
export GRB_LICENSE_FILE=$HOME/gurobi/gurobi.lic
export LD_LIBRARY_PATH=${GUROBI_HOME}/lib:${LD_LIBRARY_PATH}

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="mydogenpunk"

# Uncomment the following line to use case-sensitive completion.
 CASE_SENSITIVE="false"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
 export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
#ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="false"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git archlinux zsh-completions fzf-zsh-plugin)

autoload -U compinit && compinit

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
 export LANG=en_GB.UTF-8

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
export PATH=$HOME/Skripte/:$HOME/.local/bin:$HOME/.local/usr/bin:$HOME/.local/usr/lib:/home/sami/.gem/ruby/2.5.0/bin:${PATH}
export PYTHONPATH=$HOME/.local/usr/lib:${PYTHONPATH}

unsetopt CASE_GLOB

todo;

#COLORS IN LESS
export LESS_TERMCAP_mb=$'\E[1;31m'     # begin bold
export LESS_TERMCAP_md=$'\E[1;36m'     # begin blink
export LESS_TERMCAP_me=$'\E[0m'        # reset bold/blink
export LESS_TERMCAP_so=$'\E[01;44;33m' # begin reverse video
export LESS_TERMCAP_se=$'\E[0m'        # reset reverse video
export LESS_TERMCAP_us=$'\E[1;32m'     # begin underline
export LESS_TERMCAP_ue=$'\E[0m'        # reset underline
export GROFF_NO_SGR=1                  # for konsole and gnome-terminal

function set_bg {
    feh --bg-fill ~/bg
}

function restart_i3 {
    i3-msg restart
}

function disconnectMonitor {
    xrandr --output "$1" --off
    set_bg
    restart_i3
}

function twoMonitors {
    [ "$XDG_SESSION_TYPE" = "x11" ] && {
        FST_MONITOR="LVDS1"
            [ -z "$1" ] && SND_MONITOR="$(xrandr | sed -ne '/'$FST_MONITOR'/d'  -e 's/^\([^ ]*\) connected.*$/\1/p')" || SND_MONITOR="$1"
            SND_MONITOR_RESOLUTION="$2"

            xrandr --output LVDS1 --primary --auto --output "$SND_MONITOR" --above LVDS1 --auto
            [ ! -z "$SND_MONITOR_RESOLUTION" ] && xrandr --output "$SND_MONITOR" --mode "$SND_MONITOR_RESOLUTION" --pos 1366x0

            set_bg
            restart_i3
            i3-msg "[workspace=1] move workspace to output primary; [workspace=2] move workspace to output $SND_MONITOR"
        } || {
            echo "Error: Not compatible with Wayland"
        }
}

function connectToWifi {
    sudo nmcli con up "$*"
}

function cm {
    cmake -B build -S . -G Ninja $1 && cmake --build build -j4 $2
}
