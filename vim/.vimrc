" Make Vim more useful
set nocompatible

"make search case insensitive
set ignorecase

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
set rtp+=/usr/lib/python3.7/site-packages/powerline/bindings/vim
call plug#begin('~/.vim/plugged')

"Plug 'Valloric/YouCompleteMe' " autocomplete
Plug 'xuhdev/vim-latex-live-preview'  "latex
Plug 'tpope/vim-surround' " surround
Plug 'widatama/vim-phoenix'
Plug 'yegappan/mru' " recently used file :MRU
Plug 'kadekillary/skull-vim' "syntax highlighting
Plug 'neomake/neomake' " make with :make
Plug 'rdnetto/YCM-Generator' "helps with ycm
Plug 'flazz/vim-colorschemes' "colorschemes
Plug 'patstockwell/vim-monokai-tasty'
Plug 'scrooloose/nerdtree'
call plug#end()            " required

let g:ycm_confirm_extra_conf = 0
let g:powerline_pycmd = "py3" " else powerlines doesnt start
let g:vimtex_view_general_viewer = 'okular' "open tex pdfs in okular

"colorscheme dracula
"colorscheme skull
"colorscheme phoenix

"
let g:vim_monokai_tasty_italic = 1
let g:airline_theme='monokai_tasty'
let g:lightline = {
      \ 'colorscheme': 'monokai_tasty',
      \ }
colorscheme vim-monokai-tasty

filetype plugin indent on    " required
" Enhance command-line completion
set wildmenu
" Allow backspace in insert mode
set backspace=indent,eol,start
" Optimize for fast terminal connections
set ttyfast
" Add the g flag to search/replace by default
set gdefault
" Use UTF-8 without BOM
set encoding=utf-8 nobomb
" Change mapleader
let mapleader=","
" Don’t add empty newlines at the end of files
set binary
set noeol
" Centralize backups, swapfiles and undo history
set backupdir=~/.vim/backups
set directory=~/.vim/swaps
if exists("&undodir")
	set undodir=~/.vim/undo
endif

" Don’t create backups when editing files in certain directories
set backupskip=/tmp/*,/private/tmp/*

" Respect modeline in files
set modeline
set modelines=4
" Enable line numbers
set number
" Enable syntax highlighting
syntax on
" Make tabs as wide as 4 spaces
set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab
" Show “invisible” characters
set lcs=tab:▸\ ,trail:·,nbsp:_
set list
" Highlight dynamically as pattern is typed
set incsearch
" Always show status line
set laststatus=2
" Disable error bells
set noerrorbells
" Don’t reset cursor to start of line when moving around.
set nostartofline
" Show the cursor position
set ruler
" Don’t show the intro message when starting Vim
set shortmess=atI
" Show the current mode
set showmode
" Show the filename in the window titlebar
set title
" Show the (partial) command as it’s being typed
set showcmd
" Start scrolling three lines before the horizontal window border
set scrolloff=3

" Strip trailing whitespace (,ss)
function! StripWhitespace()
	let save_cursor = getpos(".")
	let old_query = getreg('/')
	:%s/\s\+$//e
	call setpos('.', save_cursor)
	call setreg('/', old_query)
endfunction
noremap <leader>ss :call StripWhitespace()<CR>
" Change to directory of file
nnoremap <leader>cd :cd %:p:h<CR>:pwd<CR>

" Mute C++
function! CppMute()
	let save_cursor = getpos(".")
	let old_query = getreg('/')
	:%s/^[^/]*cout.*/\/\/&/
	call setpos('.', save_cursor)
	call setreg('/', old_query)
endfunction
noremap <leader>m :call CppMute()<CR>

" Save a file as root (,W)
noremap <leader>W :w !sudo tee % <CR>:e!<CR>
" Call YcmCompleter FixIt
noremap <leader>yy :YcmCompleter FixIt<CR>
function! Format()
	let save_cursor = getpos(".")
	let old_query = getreg('/')
	:%!clang-format -style='{BasedOnStyle: Mozilla, IndentWidth: 4, FixNamespaceComments: true, ReflowComments: false}'
	call setpos('.', save_cursor)
	call setreg('/', old_query)
endfunction
noremap <leader>ff :call Format() <CR>
" jj escapes input mode
imap jj <Esc>
" better focus bindings
noremap <leader>n :NERDTreeToggle<CR>
" better tabnavigation
noremap <leader>h :tabprev<CR>
noremap <leader>l :tabnext<CR>
noremap <silent> k gk
noremap <silent> j gj


set showbreak=\ +++\         " Wrap-broken line prefix
set textwidth=0 wrapmargin=0 "DONT INSERT FUCKING NEWLINES IN MY FILES!!!
set showmatch   " Highlight matching brace
 
set incsearch   " Searches for strings incrementally
 
set autoindent  " Auto-indent new lines
set expandtab   " Use spaces instead of tabs
set shiftwidth=4        " Number of auto-indent spaces
set smartindent " Enable smart-indent
set softtabstop=4       " Number of spaces per Tab
 
"# Advanced
set ruler       " Show row and column ruler information
 
set undolevels=1000     " Number of undo levels
set backspace=indent,eol,start  " Backspace behaviour
set tildeop  " Use Tilde as an Operator
