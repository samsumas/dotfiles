#!/bin/bash

# Create virtual pulseaudiosink with:
# pacmd load-module module-null-sink sink_name=recording


RECORDING_SINK=recording
set -m # activate job control

function myecho {
    echo "*****" "$*" "*****"
}

function stoprec {
    myecho Stopping recording...
    jobs %wf-recorder >>/dev/null && kill -s SIGINT %wf-recorder
}

function timer {
    myecho "Stopping recording after sleep $*"
    sleep $* && stoprec
}

function create_sink {
    pacmd list-modules | grep -e "sink_name=$1" >>/dev/null || pacmd load-module module-null-sink sink_name="$1"
}

function rec {
    #FILENAME="$(date +%c | tr ' ' '_')_$1"
    FILENAME="${1%.*}_$(date +%c | tr ' ' '_').${1#*.}"
    shift
    create_sink "$RECORDING_SINK"
    myecho "Please check that you are using the sink '$RECORDING_SINK' for the application to be recorded!"

    ROI="$(slurp)" && myecho "Selected Region '$ROI'" || { myecho "No region selected, stopping..." && return 1; }

    wf-recorder --audio="${RECORDING_SINK}.monitor" --file="$FILENAME" -g "$ROI" 2>>/dev/null &

    myecho "Recording to $FILENAME ..."
    [ ! -z "$1" ] && timer $* || fg %wf-recorder
    return $?
}

trap "stoprec" SIGINT

rec $*